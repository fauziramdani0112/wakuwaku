var express = require('express');
var FactoryTourCtrl = require('../controllers/FactoryTour.controller')
var auth = require('../tools/middleware');
var router = express.Router();

router.get('/instansi', FactoryTourCtrl.get_Instansi)
router.get('/result/:Id', FactoryTourCtrl.get_Result)
router.get('/tour', FactoryTourCtrl.get_Tour)
router.get('/tour/:Id', FactoryTourCtrl.get_TourById)
router.post('/tour', FactoryTourCtrl.add_Tour)
router.delete('/tour/:Id', FactoryTourCtrl.delete_Tour);
router.post('/tour-multiple-delete', FactoryTourCtrl.deleteBulk_Tour);
router.put('/tour/:Id', FactoryTourCtrl.edit_Tour);
router.put('/updatetour/:Id', FactoryTourCtrl.update_StatusTour);
router.post('/join-quiz', FactoryTourCtrl.JoinTour);
router.post('/get-data-institusi', FactoryTourCtrl.getDataInstitusi);
router.post('/sign-quiz', FactoryTourCtrl.SignInQuiz);
router.post('/post-point', FactoryTourCtrl.postAnswer)

module.exports = router;