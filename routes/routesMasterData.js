var express = require('express');
var MasterDataCtrl = require('../controllers/MasterData.controller')
var auth = require('../tools/middleware');
var router = express.Router();


//Dashboard
router.get('/total-visitor', MasterDataCtrl.get_TotalVisitor)
router.get('/stat-profession', MasterDataCtrl.get_StatProfession)
router.get('/stat-gender', MasterDataCtrl.get_StatGender)
router.get('/stat-age', MasterDataCtrl.get_StatAge)

//Master Authorization
router.get('/authorizations', MasterDataCtrl.get_Authorizations)
router.post('/authorization', MasterDataCtrl.add_Authorization)
router.delete('/authorization/:Id', MasterDataCtrl.delete_Authorization);
router.post('/authorization-multiple-delete', MasterDataCtrl.deleteBulk_Authorization);
router.put('/authorization/:Id', MasterDataCtrl.edit_Authorization);

//Master Question
router.get('/category', MasterDataCtrl.get_Category)
router.get('/questions', MasterDataCtrl.get_Questions)
router.get('/questions/:Id', MasterDataCtrl.get_QuestionsId)
router.post('/question', MasterDataCtrl.add_Question)
router.delete('/question/:Id', MasterDataCtrl.delete_Question);
router.post('/question-multiple-delete', MasterDataCtrl.deleteBulk_Question);
router.put('/question/:Id', MasterDataCtrl.edit_Question);

//Master Choice
router.get('/choices/:Id', MasterDataCtrl.get_Choices)
router.post('/choice', MasterDataCtrl.add_Choice)
router.delete('/choice/:Id', MasterDataCtrl.delete_Choice);
router.post('/choice-multiple-delete', MasterDataCtrl.deleteBulk_Choice);
router.put('/choice/:Id', MasterDataCtrl.edit_Choice);

//Master Choice
router.get('/occupation', MasterDataCtrl.get_Occupation)
router.get('/packs', MasterDataCtrl.get_Packs)
router.post('/pack', MasterDataCtrl.add_Pack)
router.delete('/pack/:id', MasterDataCtrl.delete_Pack);
router.post('/pack-multiple-delete', MasterDataCtrl.deleteBulk_Pack);
router.put('/pack/:id', MasterDataCtrl.edit_Pack);

// Master Users
// router.post('/authorization', MasterDataCtrl.add_Authorization)
// router.get('/authorizations', MasterDataCtrl.get_Authorizations)
// router.get('/authorization/:id', MasterDataCtrl.get_AuthorizationByID);
// router.delete('/authorization/:id', MasterDataCtrl.delete_Authorization);
// router.put('/authorization/:id', MasterDataCtrl.edit_Authorization);
// router.get('/roles', MasterDataCtrl.get_Roles)
router.get('/autocomplete-employee/:key', MasterDataCtrl.autoCompleteEmployee)
// router.get('/autocomplete-department/:key', MasterDataCtrl.autoCompleteDepartment)
// router.get('/autocomplete-division/:key', MasterDataCtrl.autoCompleteDivision)


// Master Division
// router.post('/division', MasterDataCtrl.add_Division)
// router.get('/divisions', MasterDataCtrl.get_Divisions)
// router.get('/division/:id', MasterDataCtrl.get_DivisionByID);
// router.delete('/division/:id', MasterDataCtrl.delete_Division);
// router.put('/division/:id', MasterDataCtrl.edit_Division);


//Master department
// router.post('/department', MasterDataCtrl.add_Department)
// router.get('/departments', MasterDataCtrl.get_Departments)
// router.get('/department-pic/:nik', MasterDataCtrl.get_DepartmentPIC)
// router.get('/department/:id', MasterDataCtrl.get_DepartmentByID);
// router.delete('/department/:id', MasterDataCtrl.delete_Department);
// router.put('/department/:id', MasterDataCtrl.edit_Department);


module.exports = router;