//library
// require('dotenv').config({ path: './env/.env' });
require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bodyParser = require('body-parser');
var http = require('http');
const https = require('https');
var cors = require('cors');
var debug = require('debug')('express-template-master:server');

// routing
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var routesMasterData = require('./routes/routesMasterData');
var routesFactoryTour = require('./routes/routesFactoryTour')
var cron = require('./controllers/Scheduller.controller');
// var socket = require('./controllers/Socket.controller')
var routesAuthentication = require('./routes/routesAuthentication');
// const socketIO = require('socket.io');
//Settings
var settings = require('./config/config');

// view engine setup
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

var synologyPATH = '';
app.use(express.static(path.join(__dirname, 'app/public')));
// app.use('/app', express.static(path.join(synologyPATH)));
app.use('/app', express.static(path.join(synologyPATH)));


app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({
  limit: "50mb"
}));
app.use(bodyParser.urlencoded({
  limit: "50mb",
  extended: true,
  parameterLimit: 50000
}));

app.use(cors())
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'authorization, content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  if (req.method == 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

// path base
app.use('/api/master', routesMasterData);
app.use('/api/tour', routesFactoryTour);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', routesAuthentication);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || settings.server.port);
app.set('port', port);

/**
 * Create HTTP server.
 */

 var fs      = require('fs');
 let secure = false;
 const cert = fs.readFileSync('./ssl/aio.co.id.cer');
 const ca = fs.readFileSync('./ssl/aio.co.id.CA.cer');
 const key = fs.readFileSync('./ssl/aio.co.id.key');
 
 let options = {
  cert: cert, // fs.readFileSync('./ssl/example.crt');
  ca: ca, // fs.readFileSync('./ssl/example.ca-bundle');
  key: key // fs.readFileSync('./ssl/example.key');
};

//  https.createServer(options,app)
if (secure) {
  var server = https.createServer(options,app)
} else {
  var server = http.createServer(app);
}



// const io = socketIO(server);

const io = require("socket.io")(server, {
  cors: {
    origin: '*'
  }
})

io.on('connection', client => {

  console.log('new user connected', client.id);

  //event ketika user ada di lobby
  client.on('sign', data => {
    client.broadcast.emit('join', data);
  });

  

  client.on('showResult', data => {
    console.log(data)
    client.broadcast.emit('show_result', data);
  });

  client.on('back_to_quiz', data => {
    console.log(data)
    client.broadcast.emit('backToquiz', data);
  });


  client.on('startQuiz', data => {
    console.log(data)
    client.broadcast.emit('mulaiQuiz', data);
  });
  client.on('endQuiz', data => {
    console.log(data)
    client.broadcast.emit('selesaiQuiz', data);
  });

  

  client.on('start_question', data => {
    console.log(data)
    client.broadcast.emit('startQuestion', data);
  });

  client.on('timer', data => {
    client.broadcast.emit('timer-quiz', data);
  });

  

  //event ketika user disconnected
  client.on('disconnect', () => {
    console.log('user disconnected', client.id);
    // jika client id nya adalah client id admin update status event
  });


});
/**
 * Listen on provided port, on all network interfaces.
 */




server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// io.on('connection', (socket) => {
//   console.log('a user connected');
// });

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ?
    'Pipe ' + port :
    'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'port ' + addr.port;
  debug('Listening on ' + bind);
  console.log('Server Running On PORT : ' + port)
}

// module.exports = app;
module.exports = server;
