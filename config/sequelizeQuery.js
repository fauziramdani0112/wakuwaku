var Sequelize = require("sequelize");
const Pool = require('pg').Pool

const sequelizeIdeabox = new Sequelize(process.env.ideabox_database_name, process.env.ideabox_database_user, process.env.ideabox_database_password, {
    host: process.env.ideabox_database_host,
    dialect: process.env.ideabox_database_dialect,
    port: process.env.ideabox_database_port,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    logging: false,
    timezone: "+07:00",
    operatorsAliases: 0
});

const sequelizeHeadOffice = new Sequelize(process.env.headoffice_database_name, process.env.headoffice_database_user, process.env.headoffice_database_password, {
    host: process.env.headoffice_database_host,
    dialect: process.env.headoffice_database_dialect,
    port: process.env.headoffice_database_port,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    logging: false,
    timezone: "+07:00",
    operatorsAliases: 0
});

const sequelizeEmployee = new Sequelize(process.env.employee_database_name, process.env.employee_database_user, process.env.employee_database_password, {
    host: process.env.employee_database_host,
    dialect: process.env.employee_database_dialect,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    timezone: "+07:00",
    logging: false,
    operatorsAliases: 0,
});

const Quizz = new Pool({
	user: process.env.quizz_database_user,
	host: process.env.quizz_database_host,
	database: process.env.quizz_database_name,
	password: process.env.quizz_database_password,
	port: process.env.quizz_database_port,
  });


module.exports = {
    sequelizeHeadOffice,
    sequelizeEmployee,
    sequelizeIdeabox,
    Quizz
};