module.exports = {
    ideabox_db: {
        name: process.env.ideabox_database_name,
        username: process.env.ideabox_database_user,
        password: null,
        host: process.env.ideabox_database_host,
        dialect: process.env.ideabox_database_dialect,
        port: process.env.ideabox_database_port,
        logging: false,

    },
    server: {
        port: process.env.Port
    },
    security: {
        salt: process.env.ideabox_salt

    }
}