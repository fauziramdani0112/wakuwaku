var models = require('../config/sequelizeORM')
var sequelizeQuery = require('../config/sequelizeQuery')
var api = require('../tools/common')
var moment = require('moment')
var btoa = require('btoa');
var formidable = require('formidable');
var path = require('path');
var mv = require('mv');
var randomstring = require("randomstring");
const randomFile = require('select-random-file')
const dir = path.resolve(__dirname, '../public/avatar')

function get_Instansi(req, res) {
  sequelizeQuery.sequelizeIdeabox.query(
    `SELECT id, nama_instansi, jumlah, jadwal, site, alamat FROM rencana_kunjungan WHERE site=3 AND status=1`, {
    type: sequelizeQuery.sequelizeIdeabox.QueryTypes.SELECT
  }
  )
    .then(function (data) {
      if (data.length > 0) {
        api.ok(res, data)
      } else {
        api.error(res, 'Record not found', 200)
      }
    }).catch((e) => {
      console.log(e)
      api.error(res, e, 500)
    })
}
function get_Tour(req, res) {
  sequelizeQuery.Quizz.query(`SELECT tours.*, occupations."sortName", tbl_question_pack."PackageName" FROM tours JOIN occupations ON tours."occupation_id"=occupations."id" JOIN tbl_question_pack ON  tours."question_pack_id"=tbl_question_pack."Id" ORDER BY created desc`, (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    if (results.rows != undefined) {
      api.ok(res, results.rows) 
    } else {
      api.ok(res, [])
    }
  })
}
function get_TourById(req, res) {
  const Id = req.params.Id
  sequelizeQuery.Quizz.query(`SELECT tours.*, modes."name" AS mode, tbl_question_pack."PackageName" AS package_name, tbl_question_pack."ListQuestionId" AS list_question,tbl_question_pack."TimePerQuestion" AS time FROM tours JOIN modes ON tours.mode_id=modes."id" JOIN tbl_question_pack ON tours.question_pack_id = tbl_question_pack."Id" WHERE tours.id=$1`, [Id], (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    if (results.rows != undefined) {
      api.ok(res, results.rows)
    } else {
      api.ok(res, [])
    }
  })
}
function add_Tour(req, res) {
  // console.log(req.body);
  
  const d = new Date();
  var kosong = ''
  let year = d.getFullYear();
  let month = d.getMonth() + 1;
  let date = d.getDate();
  if (month <= 9) {
    kosong = 0
  }
  var idRandom = randomstring.generate({
    length: 3,
    charset: 'alphabetic',
    capitalization: 'lowercase'
  });
  var codeRandom = randomstring.generate({
    length: 5,
    charset: 'numeric'
  });
  let tour = {
    id: idRandom + year + kosong + month + date,
    mode_id: req.body.mode_id,
    created: req.body.created,
    code: codeRandom,
    institution_name: req.body.institution_name,
    total_visitor: req.body.total_visitor,
    address: req.body.address,
    site: req.body.site,
    tipe: req.body.tipe,
    occupation_id: req.body.occupation_id,
    question_pack_id: req.body.question_pack_id
  }
  const { id, mode_id, created, code, institution_name, total_visitor, address, site, tipe, occupation_id, question_pack_id } = tour
  sequelizeQuery.Quizz
    .query('INSERT INTO tours ("id", "mode_id", "created", "code", "institution_name", "total_visitor", "address", "site", "tipe", "occupation_id", "question_pack_id") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *', [id, mode_id, created, code, institution_name, total_visitor, address, site, tipe, occupation_id, question_pack_id], (error, results) => {
      if (error) {
        throw error
      }
      if (results.rows != undefined) {
        api.ok(res, results.rows)
      } else {
        api.ok(res, [])
      }
    })
}
function edit_Tour(req, res) {
  const id = req.params.Id
  console.log("id :", id)
  const { mode_id, created, code, institution_name, total_visitor, address, site, tipe, occupation_id, question_pack_id } = req.body

  sequelizeQuery.Quizz
    .query('UPDATE tours SET "mode_id" = $1, "created" = $2, "code" = $3, "institution_name"=$4, "total_visitor"=$5, "address"=$6, "site"=$7, "tipe"=$8, "occupation_id"=$9, "question_pack_id"=$10 WHERE "id"=$11', [mode_id, created, code, institution_name, total_visitor, address, site, tipe, occupation_id, question_pack_id, id], (error, results) => {
      if (error) {
        throw error
      }
      if (results.rows != undefined) {
        api.ok(res, results.rows)
      } else {
        api.ok(res, [])
      }
    })
}

function delete_Tour(req, res) {
  const id = req.params.Id
  console.log("delete satu : ", id);
  sequelizeQuery.Quizz
    .query('DELETE FROM tours WHERE "id"=$1', [id], (error, results) => {
      if (error) {
        throw error
      }
      if (results.rows != undefined) {
        api.ok(res, results.rows)
      } else {
        api.ok(res, [])
      }
    })
}
function deleteBulk_Tour(req, res) {
  for (let i = 0; i < req.body.id.length; i++) {
    const id = req.body.id[i]
    sequelizeQuery.Quizz
      .query('DELETE FROM tours WHERE "id" IN ($1)', [id], (error, results) => {
        if (error) {
          throw error
        }
        if (i == req.body.id.length - 1) {
          if (results.rows != undefined) {
            api.ok(res, results.rows)
          } else {
            api.ok(res, [])
          }
        }
        console.log("delete id", req.body.id[i])
      })
  }
}
function get_Result(req, res) {
  const Id = req.params.Id
  sequelizeQuery.Quizz.query(`SELECT nickname,points,avatar FROM "users" WHERE tour_id=$1 ORDER BY points DESC LIMIT 8`, [Id], (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    if (results.rows != undefined) {
      api.ok(res, results.rows)
    } else {
      api.ok(res, [])
    }
  })
}

function JoinTour(req, res) {
  const code = req.body.code;
  // let code = '11497'
  sequelizeQuery.Quizz.query('SELECT * FROM tours WHERE "status" = 1' + 'AND "code" = $1', [code], (error, results) => {
    if (error) {
      // console.log(error)
      api.error(res, error, 500)
    }

    // console.log(results)
    if (results.rows != undefined) {
      api.ok(res, results.rows)
    } else {
      api.ok(res, [])
    }
  })
}
function getDataInstitusi(req, res) {
  let id = req.body.id
  sequelizeQuery.Quizz.query('SELECT * FROM tours WHERE "id" = $1', [id], (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    // console.log(results)
    if (results.rows != undefined) {
      api.ok(res, results.rows)
    } else {
      api.ok(res, [])
    }
  })
}
async function SignInQuiz(req, res) {
  console.log("panggill sayaa");
  randomFile(dir, (err, file) => {

    let dirFile = '/public/avatar/' + file
    const d = new Date();
    var kosong = ''
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    let date = d.getDate();

    if (month <= 9) {
      kosong = 0
    }
    var idRandom = randomstring.generate({
      length: 3,
      charset: 'alphabetic',
      capitalization: 'lowercase'
    });
    var idRandomsuff = randomstring.generate({
      length: 2,
      charset: 'alphabetic',
      capitalization: 'lowercase'
    });


    let tour = {
      id: idRandom + year + kosong + month + date + idRandomsuff,
      name: req.body.name,
      nickname: req.body.nickname,
      email: req.body.email,
      tour_id: req.body.tour_id,
      created: moment().format("YYYY-MM-DD H:mm:ss"),
      phone_number: req.body.phonenumber,
      avatar: dirFile,
      gender_id: req.body.gender,
      birthday: moment().format("YYYY-MM-DD"),
      badge_scale: 1,
    }

    console.log("datttaa tour :", tour);
    const { id, name, nickname, email, tour_id, created, phone_number, avatar, gender_id, birthday, badge_scale } = tour

    sequelizeQuery.Quizz
      .query('INSERT INTO users ("id", "name", "nickname", "email", "tour_id", "created", "phone_number", "avatar", "gender_id", "birthday", "badge_scale") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *', [id, name, nickname, email, tour_id, created, phone_number, avatar, gender_id, birthday, badge_scale], (error, results) => {
        if (error) {
          console.log(error)
          throw error
        }
        api.ok(res, results.rows)
      })


  });
}
function update_StatusTour(req, res) {
  const id = req.params.Id
  console.log("id :", id)
  const { status } = req.body

  sequelizeQuery.Quizz
    .query('UPDATE tours SET "status" = $1 WHERE "id"=$2', [status, id], (error, results) => {
      if (error) {
        throw error
      }
      if (results.rows != undefined) {
        api.ok(res, results.rows)
      } else {
        api.ok(res, [])
      }
    })
}

async function postAnswer(req, res) {
  // console.log(req.body);

  let obj_point = {
    point: req.body.point,
    user: req.body.user,
    tour: req.body.tour,
    nickname: req.body.nickname,
    question_id: req.body.question_id,
  }
  const { point, user, tour, nickname, question_id } = obj_point


  try {

    var usersData = await sequelizeQuery.Quizz
      .query(`SELECT * FROM "users" WHERE "id" = '${user}'`,);

    console.log(usersData.rows);
    var usersAnswer = await sequelizeQuery.Quizz
      .query(`SELECT * FROM "tbl_answer" WHERE "userId" = '${user}' AND "questionId" = '${question_id}' AND "tourId" = '${tour}'`,);

    // console.log(usersAnswer.rows);

    if (usersAnswer.rows.length == 0) {

      var doc = await sequelizeQuery.Quizz
        .query('INSERT INTO tbl_answer ("userId", "questionId", "tourId", "userName", "point") VALUES ($1, $2, $3, $4, $5) RETURNING *', [user, question_id, tour, nickname, point])
      console.log(doc.rows);
      let nilai = Number(usersData.rows[0].points) + Number(point);
      console.log('ini nilai', Number(nilai))
      let balikan = {
        point : Number(nilai).toFixed(2)
      }
      sequelizeQuery.Quizz
        .query('UPDATE "users" SET "points" = $1 WHERE "id"=$2', [Number(nilai).toFixed(2), user], (error, results) => {
          if (error) {  
            throw error
          }
          if (results.rows != undefined) {
            console.log(results.rows)
            api.ok(res, balikan)
          } else {
            api.ok(res, [])
          }
        })

    } else {
      api.ok(res, {point:usersData.rows[0].points})
    }


  } catch (e) {
    console.log(e);
    api.error(res, e, 500)
  }

  // const id = req.params.Id
  // console.log("id :", id)
  // const { status } = req.body

  // sequelizeQuery.Quizz
  //   .query('UPDATE tours SET "status" = $1 WHERE "id"=$2', [status, id], (error, results) => {
  //     if (error) {
  //       throw error
  //     }
  //     if(results.rows != undefined){
  //     api.ok(res, results.rows)
  //   }else{
  //     api.ok(res, [])
  //   }
  //   })
}

module.exports = {
  get_Instansi,
  get_Tour,
  add_Tour,
  edit_Tour,
  delete_Tour,
  deleteBulk_Tour,
  get_TourById,
  get_Result,
  JoinTour,
  getDataInstitusi,
  SignInQuiz,
  update_StatusTour,
  postAnswer
};