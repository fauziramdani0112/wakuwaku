var models = require('../config/sequelizeORM')
var sequelizeQuery = require('../config/sequelizeQuery')
var api = require('../tools/common')
var moment = require('moment')
var btoa = require('btoa');
var formidable = require('formidable');
var path = require('path');
var mv = require('mv');
// Masterdata Authorization

function get_Authorizations(req, res) {
  sequelizeQuery.Quizz.query(`SELECT * FROM mst_authorization`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function add_Authorization(req, res) {
  const { EmployeeCode, FullName, Email, MobileNumber, Role, isActive, DateCreated } = req.body

    sequelizeQuery.Quizz
    .query('INSERT INTO mst_authorization ("EmployeeCode", "FullName", "Email", "MobileNumber", "Role", "isActive", "DateCreated") VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *', [EmployeeCode, FullName, Email, MobileNumber, Role, isActive, DateCreated], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  function edit_Authorization(req, res) {
    const Id = parseInt(req.params.Id)
    const { EmployeeCode, FullName, Email, MobileNumber, Role, isActive, DateCreated } = req.body

    sequelizeQuery.Quizz
    .query('UPDATE mst_authorization SET "EmployeeCode" = $1, "FullName" = $2, "Email" = $3, "MobileNumber"=$4, "Role"=$5, "isActive"=$6, "DateCreated"=$7 WHERE "Id"=$8', [EmployeeCode, FullName, Email, MobileNumber, Role, isActive, DateCreated, Id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  
  function delete_Authorization(req, res) {
    const Id = parseInt(req.params.Id)
    console.log("delete satu : ",req.params.Id);

    sequelizeQuery.Quizz
    .query('DELETE FROM mst_authorization WHERE "Id"=$1', [Id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  function deleteBulk_Authorization(req, res) {
    for (let i = 0; i < req.body.Id.length; i++) {
      const Id = parseInt(req.body.Id[i])
      sequelizeQuery.Quizz
      .query('DELETE FROM mst_authorization WHERE "Id" IN ($1)', [Id], (error, results) => {
        if (error) {
          throw error
        }
        if (i == req.body.Id.length - 1) {
          api.ok(res, results.rows)
        }
        console.log("delete id",req.body.Id[i])
      })
    }
  }

function get_Category(req, res) {
  sequelizeQuery.Quizz.query(`SELECT * FROM tbl_category_quiz`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function get_Questions(req, res) {
  sequelizeQuery.Quizz.query(`SELECT tbl_question_quiz.*, tbl_category_quiz."CategoryName" FROM tbl_question_quiz JOIN tbl_category_quiz ON tbl_category_quiz."Id"= tbl_question_quiz."Category" ORDER BY "Id" ASC`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function get_QuestionsId(req, res) {
  const Id = parseInt(req.params.Id)
  sequelizeQuery.Quizz.query(`SELECT * FROM tbl_question_quiz WHERE "Id"=$1`, [Id], (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    api.ok(res, results.rows)
  })
}

async function add_Question(req, res) {
  var form = new formidable.IncomingForm();
  let questcode = await sequelizeQuery.Quizz.query(`SELECT "QuestionCode" FROM tbl_question_quiz ORDER BY "Id" DESC LIMIT 1`,
  )
  .then(function (data) {
    return data.rows;
  })
  .catch(function (e) {
    // api.error(res, e, 500);
    console.log(e);
  });
  let quest_number = RunningNumber(questcode[0].QuestionCode);
  
  var formfields = await new Promise(function (resolve, reject) {
    form.parse(req, async function (err, fields, files) {
      if (files.ImageUrl != undefined) {
        var reference_calculation_oldpath = files.ImageUrl.path;
        var reference_calculation_newpath =  'public/upload/' + quest_number + path.extname(files.ImageUrl.name)
        mv(reference_calculation_oldpath, reference_calculation_newpath, function (err) {
          if (err) throw err;
        });
      }
      let quiz = {
        Question : fields.Question,
        IsActive : fields.IsActive,
        Category : fields.Category,
        ImageUrl : reference_calculation_newpath,
        QuestionCode : quest_number
      }
      const { Question, IsActive, Category, ImageUrl, QuestionCode } = quiz
      await sequelizeQuery.Quizz
      .query('INSERT INTO tbl_question_quiz ("Question", "IsActive", "Category", "ImageUrl", "QuestionCode") VALUES ($1, $2, $3, $4, $5) RETURNING *', [Question, IsActive, Category, ImageUrl, QuestionCode], (error, results) => {
        if (error) {
          throw error
        }
        api.ok(res, results.rows)
      })
      resolve(fields);
    });
  });
  }
  async function edit_Question(req, res) {
    var form = new formidable.IncomingForm();
    const Id = parseInt(req.params.Id)
    var formfields = await new Promise(function (resolve, reject) {
      form.parse(req, async function (err, fields, files) {
        if (files.ImageUrl != undefined) {
          var reference_calculation_oldpath = files.ImageUrl.path;
          var reference_calculation_newpath =  'public/upload/' + fields.QuestionCode + path.extname(files.ImageUrl.name)
          mv(reference_calculation_oldpath, reference_calculation_newpath, function (err) {
            if (err) throw err;
          });
          let quiz = {
            Question : fields.Question,
            IsActive : fields.IsActive,
            Category : fields.Category,
            ImageUrl : reference_calculation_newpath,
            QuestionCode : fields.QuestionCode
          }
          const { Question, IsActive, Category, ImageUrl, QuestionCode } = quiz
          await sequelizeQuery.Quizz
          .query('UPDATE tbl_question_quiz SET "Question" = $1, "IsActive" = $2, "Category" = $3, "ImageUrl"=$4, "QuestionCode"=$5 WHERE "Id"=$6', [Question, IsActive, Category, ImageUrl, QuestionCode, Id], (error, results) => {
            if (error) {
              throw error
            }
            api.ok(res, results.rows)
          })
        }else if(files.ImageUrl == undefined){
          let quiz = {
            Question : fields.Question,
            IsActive : fields.IsActive,
            Category : fields.Category,
            QuestionCode : fields.QuestionCode
          }
          const { Question, IsActive, Category, QuestionCode } = quiz
          await sequelizeQuery.Quizz
          .query('UPDATE tbl_question_quiz SET "Question" = $1, "IsActive" = $2, "Category" = $3, "QuestionCode"=$4 WHERE "Id"=$5', [Question, IsActive, Category, QuestionCode, Id], (error, results) => {
            if (error) {
              throw error
            }
            api.ok(res, results.rows)
          })
        }
        
        resolve(fields);
      });
    });
  }
  function RunningNumber(data) {

    // let str = "002/IFI/AIO/06/2022";
    // tslint:disable-next-line: one-variable-per-declaration
    const format = 'PS';

    let array = [];
    let no_runner;

    if (data != null) {
        array = data.split('-');
        no_runner = array[1];
        no_runner++;
    } else {
        no_runner = 1
    }
    if (no_runner <= 9) {
        no_runner = '0' + no_runner;
    } else if (no_runner > 9 && no_runner <= 99) {
        no_runner = no_runner;
    }

    let running = format + '-' + no_runner ;
    return running;
}
  
  function delete_Question(req, res) {
    const Id = parseInt(req.params.Id)

    sequelizeQuery.Quizz
    .query('DELETE FROM tbl_question_quiz WHERE "Id"=$1', [Id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  function deleteBulk_Question(req, res) {
    for (let i = 0; i < req.body.Id.length; i++) {
      const Id = parseInt(req.body.Id[i])
      sequelizeQuery.Quizz
      .query('DELETE FROM tbl_question_quiz WHERE "Id" IN ($1)', [Id], (error, results) => {
        if (error) {
          throw error
        }
        if (i == req.body.Id.length - 1) {
          api.ok(res, results.rows)
        }
        console.log("delete id",req.body.Id[i])
      })
    }
  }

function get_Choices(req, res) {
  const Id = parseInt(req.params.Id)

  sequelizeQuery.Quizz.query(`SELECT * FROM tbl_question_choice WHERE "Question_Id"=$1`, [Id], (error, results) => {
    if (error) {
      api.error(res, error, 500)
    }
    api.ok(res, results.rows)
  })
}
async function add_Choice(req, res) {
  const { Question_Id, IsRight, Choice_Desc } = req.body
  if (req.body.IsRight == true) {
    await sequelizeQuery.Quizz
    .query('UPDATE tbl_question_choice SET "IsRight" = false WHERE "Question_Id"=$1', [Question_Id], (error, results) => {
      if (error) {
        throw error
      }
      console.log("succes update")
    })
  }
    await sequelizeQuery.Quizz
    .query('INSERT INTO tbl_question_choice ("Question_Id", "IsRight", "Choice_Desc") VALUES ($1, $2, $3) RETURNING *', [Question_Id, IsRight, Choice_Desc], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  async function edit_Choice(req, res) {
    const Id = parseInt(req.params.Id)
    const { Question_Id, IsRight, Choice_Desc } = req.body
    if (req.body.IsRight == true) {
      await sequelizeQuery.Quizz
      .query('UPDATE tbl_question_choice SET "IsRight" = false WHERE "Question_Id"=$1', [Question_Id], (error, results) => {
        if (error) {
          throw error
        }
        console.log("succes update")
      })
    }
    await sequelizeQuery.Quizz
    .query('UPDATE tbl_question_choice SET "Question_Id" = $1, "IsRight" = $2, "Choice_Desc" = $3 WHERE "Id"=$4', [Question_Id, IsRight, Choice_Desc, Id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  
  function delete_Choice(req, res) {
    const Id = parseInt(req.params.Id)

    sequelizeQuery.Quizz
    .query('DELETE FROM tbl_question_choice WHERE "Id"=$1', [Id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  function deleteBulk_Choice(req, res) {
    for (let i = 0; i < req.body.Id.length; i++) {
      const Id = parseInt(req.body.Id[i])
      sequelizeQuery.Quizz
      .query('DELETE FROM tbl_question_choice WHERE "Id" IN ($1)', [Id], (error, results) => {
        if (error) {
          throw error
        }
        if (i == req.body.Id.length - 1) {
          api.ok(res, results.rows)
        }
        console.log("delete id",req.body.Id[i])
      })
    }
  }

function get_Occupation(req, res) {
  sequelizeQuery.Quizz.query(`SELECT * FROM occupations`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function get_Packs(req, res) {
  sequelizeQuery.Quizz.query(`SELECT tbl_question_pack.*, occupations."sortName" FROM tbl_question_pack JOIN occupations ON tbl_question_pack."OccupationId"=occupations."id"`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function add_Pack(req, res) {
  const { PackageName, OccupationId, IsActive, ListQuestionId, TimePerQuestion } = req.body
    sequelizeQuery.Quizz
    .query('INSERT INTO tbl_question_pack ("PackageName", "OccupationId", "IsActive", "ListQuestionId","TimePerQuestion") VALUES ($1, $2, $3, $4, $5) RETURNING *', [PackageName, OccupationId, IsActive, ListQuestionId, TimePerQuestion], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  async function edit_Pack(req, res) {
    const id = parseInt(req.params.id)
    const { PackageName, OccupationId, IsActive, ListQuestionId, TimePerQuestion  } = req.body
    await sequelizeQuery.Quizz
    .query('UPDATE tbl_question_pack SET "PackageName" = $1, "OccupationId" = $2, "IsActive" = $3, "ListQuestionId"=$4, "TimePerQuestion"=$5 WHERE "Id"=$6', [PackageName, OccupationId, IsActive, ListQuestionId, TimePerQuestion, id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  
  function delete_Pack(req, res) {
    const id = parseInt(req.params.id)

    sequelizeQuery.Quizz
    .query('DELETE FROM tbl_question_pack WHERE "Id"=$1', [id], (error, results) => {
      if (error) {
        throw error
      }
      api.ok(res, results.rows)
    })
  }
  function deleteBulk_Pack(req, res) {
    for (let i = 0; i < req.body.Id.length; i++) {
      const Id = parseInt(req.body.Id[i])
      sequelizeQuery.Quizz
      .query('DELETE FROM tbl_question_pack WHERE "Id" IN ($1)', [Id], (error, results) => {
        if (error) {
          throw error
        }
        if (i == req.body.Id.length - 1) {
          api.ok(res, results.rows)
        }
        console.log("delete Id",req.body.Id[i])
      })
    }
  }

// function get_Authorizations(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `SELECT a.id, a.nik, b.employee_name, a.created_at, a.role_id, c.role_name, a.status  FROM mst_authorization a LEFT JOIN aio_employee.mst_employment b ON CONCAT('0',a.nik) = b.employee_code LEFT JOIN mst_role c ON a.role_id = c.id`, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   }
//   )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }

// function edit_Authorization(req, res) {
//   models.mst_authorization.update(
//     req.body, {
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(data => {
//       api.ok(res, req.body)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }

// function delete_Authorization(req, res) {
//   models.mst_authorization.destroy({
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         api.ok(res, deletedRecord)
//       } else {
//         api.error(res, 'Record not found', 200);
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }


// function get_AuthorizationByID(req, res) {
//   models.mst_authorization.findAll({
//     where: {
//       id: req.params.id
//     },
//     logging: console.log
//   }).then(data => {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }



// Masterdata Division

// async function add_Division(req, res) {
//   console.log(req.body);
//   let folder_path = 'Public/' + req.body.division_desc

//   const stamp = String(Date.now()).split('').reverse().join('');
//   let url = btoa(stamp + req.body.created_by).replace(/[^a-zA-Z0-9 ]/g, '');
//   console.log(url)

//   var addFolder = await sequelizeQuery.sequelizeDMS.query(
//     `
//     INSERT INTO tbl_folders 
//     ( folder_path, folder_name, folder_url, folder_parent, owner, tag, created_at, updated_at, pin_shared )
//     VALUES
//     ( 
//       '${folder_path}', '${req.body.division_desc}', '${url}', 
//       '${0}', '${req.body.created_by}', '${'division, public'}',
//       CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '${0}'
//     )
//     `, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.INSERT
//   }).then(function (data) {
//     if (data.length > 0) {
//       return data
//     }
//   }).catch((e) => {
//     console.log(e)
//   })

//   let obj_access = {
//     folder_id: addFolder[0],
//     subject_type: 'division',
//     subject: req.body.division_code,
//     access_level: 'READ,DOWNLOAD,WRITE'
//   }

//   var add_access = await models.tbl_folder_access.create(obj_access)
//     .then(function (create) {
//       return create
//     }).catch((e) => {
//       console.log(e)
//     })

//   let obj_division = {
//     division_code: req.body.division_code,
//     division_desc: req.body.division_desc,
//     created_by: req.body.created_by,
//     is_active: req.body.is_active,
//     created_at: req.body.created_at,
//     folder_id: addFolder[0]
//   }

//   models.mst_division.create(obj_division)
//     .then(function (create) {
//       api.ok(res, create)
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }

// function edit_Division(req, res) {
//   models.mst_division.update(
//     req.body, {
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(data => {
//       api.ok(res, req.body)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }

// function delete_Division(req, res) {
//   models.mst_division.destroy({
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         api.ok(res, deletedRecord)
//       } else {
//         api.error(res, 'Record not found', 200);
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }


// function get_DivisionByID(req, res) {
//   models.mst_division.findAll({
//     where: {
//       id: req.params.id
//     },
//     logging: console.log
//   }).then(data => {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }

// function get_Divisions(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `SELECT a.*, b.folder_path FROM mst_division a LEFT JOIN tbl_folders b ON a.folder_id = b.id`, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   }
//   )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }


// Masterdata Department

// async function add_Department(req, res) {
//   console.log(req.body);
//   let folder_path = 'Public/' + req.body.division_desc + '/' + req.body.department_name
//   const stamp = String(Date.now()).split('').reverse().join('');
//   let url = btoa(stamp + req.body.created_by).replace(/[^a-zA-Z0-9 ]/g, '');
//   var addFolder = await sequelizeQuery.sequelizeDMS.query(
//     `
//     INSERT INTO tbl_folders 
//     ( folder_path, is_department, folder_name, folder_url, folder_parent, owner, tag, created_at, updated_at, pin_shared )
//     VALUES
//     ( 
//       '${folder_path}', '1' , '${req.body.department_name}', '${url}', 
//       '${req.body.folder_parent}', '${req.body.created_by}', '${'department, public'}',
//       CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '${0}'
//     )
//     `, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.INSERT
//   }).then(function (data) {
//     if (data.length > 0) {
//       return data
//     }
//   }).catch((e) => {
//     console.log(e)
//   })

//   let obj_access = {
//     folder_id: addFolder[0],
//     subject_type: 'department',
//     subject: req.body.department_code,
//     access_level: 'READ,DOWNLOAD,WRITE'
//   }

//   var add_access = await models.tbl_folder_access.create(obj_access)
//     .then(function (create) {
//       return create
//     }).catch((e) => {
//       console.log(e)
//     });

//   let obj_department = {
//     department_code: req.body.department_code,
//     department_name: req.body.department_name,
//     created_by: req.body.created_by,
//     is_active: req.body.is_active,
//     created_at: req.body.created_at,
//     folder_id: addFolder[0],
//     division_code: req.body.division_code,
//     division_desc: req.body.division_desc
//   }

//   models.mst_department.create(obj_department)
//     .then(function (create) {
//       api.ok(res, create)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }



// function edit_Department(req, res) {
//   models.mst_department.update(
//     req.body, {
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(data => {
//       api.ok(res, req.body)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }

// function delete_Department(req, res) {
//   models.mst_department.destroy({
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         api.ok(res, deletedRecord)
//       } else {
//         api.error(res, 'Record not found', 200);
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }


// function get_DepartmentByID(req, res) {
//   models.mst_department.findAll({
//     where: {
//       id: req.params.id
//     },
//     logging: console.log
//   }).then(data => {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }

// function get_Departments(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `SELECT a.id, a.department_name, a.department_code, a.division_code, a.created_at , a.folder_id, b.division_desc, c.folder_path FROM mst_department a LEFT JOIN mst_division b ON a.division_code = b.division_code LEFT JOIN tbl_folders c ON a.folder_id = c.id ORDER BY b.division_desc`, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   }
//   )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })

// }

// function get_DepartmentPIC(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `SELECT a.id, a.department_name, a.department_code, a.division_code, a.created_at , a.folder_id, b.division_desc, c.folder_path FROM mst_department a LEFT JOIN mst_division b ON a.division_code = b.division_code LEFT JOIN tbl_folders c ON a.folder_id = c.id LEFT JOIN tbl_pic d ON c.id = d.folder_id WHERE c.is_department = '1' AND d.nik = '${req.params.nik}'  ORDER BY b.division_desc`, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   }
//   )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })

// }

// function get_Brands(req, res) {
//   models.tbl_brand.findAll()
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, "Data Kosong", 200)
//       }
//     }).catch(function (e) {
//       api.error(res, e, 500)
//     })
// }

//Master Role

// function add_Role(req, res) {
//   models.mst_role.create(req.body)
//     .then(function (create) {
//       api.ok(res, create)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }



// function edit_Role(req, res) {
//   models.mst_role.update(
//     req.body, {
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(data => {
//       api.ok(res, req.body)
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }

// function delete_Role(req, res) {
//   models.mst_role.destroy({
//     where: {
//       id: req.params.id
//     }
//   })
//     .then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         api.ok(res, deletedRecord)
//       } else {
//         api.error(res, 'Record not found', 200);
//       }
//     }).catch((e) => {
//       api.error(res, e, 500)
//     })
// }


// function get_RoleByID(req, res) {
//   models.mst_role.findAll({
//     where: {
//       id: req.params.id
//     },
//     logging: console.log
//   }).then(data => {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }

// function get_Roles(req, res) {
//   models.mst_role.findAll()
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, "Data Kosong", 200)
//       }
//     }).catch(function (e) {
//       api.error(res, e, 500)
//     })
// }


// function autoCompleteEmployee(req, res) {
//   sequelizeQuery.sequelizeEmployee.query(
//     "SELECT TOP 10 lg_nik, lg_name FROM PHP_ms_login where (lg_name LIKE '%" + req.params.key + "%' OR lg_nik LIKE '%" + req.params.key + "%')", {
//     type: sequelizeQuery.sequelizeEmployee.QueryTypes.SELECT
//   })
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, "Data Kosong", 200)
//       }
//     }).catch(function (e) {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }
function autoCompleteEmployee(req, res) {
  sequelizeQuery.sequelizeHeadOffice.query(
    `SELECT RIGHT
    (Employee_Code,4) AS Nik, 
    Employee_Name AS Name,
    phone_number AS PhoneNumber,
    mail_id AS Email,
  CASE      
      WHEN org_locn_work_code = 'WL003' THEN
      'Sukabumi' 
      WHEN org_locn_work_code = 'WL004' THEN
      'Kejayan' ELSE 'Head Office'  END AS Site
    FROM mst_employment
    WHERE employee_code 
    LIKE '%${req.params.key}%' OR Employee_Name LIKE '%${req.params.key}%' ORDER BY Employee_Code DESC LIMIT 20
    `, {
    type: sequelizeQuery.sequelizeHeadOffice.QueryTypes.SELECT
  }
  )
    .then(function (data) {
      if (data.length > 0) {
        api.ok(res, data)
      } else {
        api.error(res, 'Record not found', 200)
      }
    }).catch((e) => {
      console.log(e)
      api.error(res, e, 500)
    })
}

// Dashboard
function get_StatGender(req, res) {
  sequelizeQuery.Quizz.query(`SELECT COUNT(*), gender_id FROM users GROUP BY gender_id`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function get_StatProfession(req, res) {
  sequelizeQuery.Quizz.query(`SELECT COUNT(*), occupation_id FROM users GROUP BY occupation_id ORDER BY occupation_id`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
function get_TotalVisitor(req, res) {
  console.log("MASUKK");
  sequelizeQuery.Quizz.query(`SELECT COUNT(*) AS total FROM users`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}

function get_StatAge(req, res) {
  console.log("MASUKK");
  sequelizeQuery.Quizz.query(`SELECT SUM
	( CASE WHEN CURRENT_DATE - birthday < 3652.5 THEN 1 ELSE 0 END ) AS age10,
	SUM ( CASE WHEN CURRENT_DATE - birthday BETWEEN 3652.5 AND 7305 THEN 1 ELSE 0 END ) AS age1020,
	SUM ( CASE WHEN CURRENT_DATE - birthday BETWEEN 7305 AND 10957.5 THEN 1 ELSE 0 END ) AS age2030,
	SUM ( CASE WHEN CURRENT_DATE - birthday BETWEEN 10957.5 AND 14610 THEN 1 ELSE 0 END ) AS age3040,
	SUM ( CASE WHEN CURRENT_DATE - birthday BETWEEN 14610 AND 18262.5 THEN 1 ELSE 0 END ) AS age4050,
	SUM ( CASE WHEN CURRENT_DATE - birthday BETWEEN 18262.5 AND 21915 THEN 1 ELSE 0 END ) AS age5060,
	SUM ( CASE WHEN CURRENT_DATE - birthday > 21915 THEN 1 ELSE 0 END ) AS age60 
FROM
	users`, (error, results) => {
    if (error) {
      api.error(res, e, 500)
    }
    api.ok(res, results.rows)
  })
}
// function autoCompleteDepartment(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     "SELECT * FROM mst_department where (department_name LIKE '%" + req.params.key + "%' OR department_code LIKE '%" + req.params.key + "%') LIMIT 10", {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   })
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, "Data Kosong", 200)
//       }
//     }).catch(function (e) {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }

// function autoCompleteDivision(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     "SELECT * FROM mst_division where (division_desc LIKE '%" + req.params.key + "%' OR division_code LIKE '%" + req.params.key + "%') LIMIT 10", {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   })
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, "Data Kosong", 200)
//       }
//     }).catch(function (e) {
//       console.log(e)
//       api.error(res, e, 500)
//     })
// }

// function get_PicByFolder(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `SELECT a.id, a.folder_id, a.nik, c.employee_name FROM tbl_pic a LEFT JOIN mst_department b ON a.folder_id = b.folder_id LEFT JOIN aio_employee.mst_employment c ON CONCAT('0',a.nik) = c.employee_code WHERE a.folder_id ='${req.params.folder_id}'`, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.SELECT
//   }
//   )
//     .then(function (data) {
//       if (data.length > 0) {
//         api.ok(res, data)
//       } else {
//         api.error(res, 'Record not found', 200)
//       }
//     }).catch((e) => {
//       console.log(e)
//       api.error(res, e, 500)
//     })

// }

// function add_Pic(req, res) {
//   let values = [];
//   req.body.data_emp.forEach(element => {
//     values.push(`(${element.folder_id}, "${element.nik}", "${req.body.created.created_at}", "${req.body.created.user_created}")`);
//   });
//   values.join(",")
//   console.log(values);
//   sequelizeQuery.sequelizeDMS.query(
//     `
//       INSERT INTO tbl_pic 
//       (folder_id, nik, created_at, created_by)
//       VALUES
//       ` + values, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.INSERT
//   }
//   ).then(function (data) {
//     if (data.length > 0) {
//       api.ok(res, data)
//     } else {
//       api.error(res, 'Record not found', 200)
//     }
//   }).catch((e) => {
//     console.log(e)
//     api.error(res, e, 500)
//   })

// }

// function delete_Pic(req, res) {
//   sequelizeQuery.sequelizeDMS.query(
//     `
//       DELETE FROM tbl_pic 
//       WHERE folder_id = ${req.body.id}
//       `, {
//     type: sequelizeQuery.sequelizeDMS.QueryTypes.DELETE
//   }
//   ).then(function (deletedRecord) {
//     if (deletedRecord === 1) {
//       api.ok(res, deletedRecord)
//     } else {
//       api.error(res, 'Record not found', 200);
//     }
//   }).catch((e) => {
//     api.error(res, e, 500)
//   })
// }


module.exports = {
  get_Authorizations,
  add_Authorization,
  edit_Authorization,
  delete_Authorization,
  deleteBulk_Authorization,
  autoCompleteEmployee,
  get_Category,
  get_Questions,
  get_QuestionsId,
  add_Question,
  edit_Question,
  delete_Question,
  deleteBulk_Question,
  get_Choices,
  add_Choice,
  edit_Choice,
  delete_Choice,
  deleteBulk_Choice,
  get_Occupation,
  get_Packs,
  add_Pack,
  edit_Pack,
  delete_Pack,
  deleteBulk_Pack,
  get_StatGender,
  get_StatProfession,
  get_TotalVisitor,
  get_StatAge
};