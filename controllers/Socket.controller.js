var {server, app} = require('../app.js');

// const { Server } = require("socket.io");
// const io = new Server(server);

// const io = require("socket.io")(server, {
//     allowRequest: (req, callback) => {
//       callback(null, false);
//     }
//   });
var port = normalizePort(process.env.PORT || settings.server.port);
app.set('port', port);

const io = require("socket.io")(server, {
    cors : {
        origin:'*'
    }
})

io.on('connection', (socket) => {
  console.log('a user connected');
});
function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }
  

server.listen(port);