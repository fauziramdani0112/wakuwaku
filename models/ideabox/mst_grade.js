const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_grade', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Level: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    min: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    max: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Point: {
      type: DataTypes.DECIMAL(65,0),
      allowNull: true
    },
    Reward: {
      type: DataTypes.DECIMAL(65,0),
      allowNull: true
    },
    CreatedBy: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateModified: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mst_grade',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
