const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_job_owner', {
    Id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    JobOwnerGroup: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    isSuper: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    isTechRelated: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    UserJobOwner: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateModified: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedBy: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mst_job_owner',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Id" },
        ]
      },
    ]
  });
};
