var DataTypes = require("sequelize").DataTypes;
var _mst_approval_level = require("./mst_approval_level");
var _mst_area_implementation = require("./mst_area_implementation");
var _mst_area_implementation_1 = require("./mst_area_implementation_1");
var _mst_area_implementation_2 = require("./mst_area_implementation_2");
var _mst_area_type = require("./mst_area_type");
var _mst_aspect = require("./mst_aspect");
var _mst_authorization = require("./mst_authorization");
var _mst_configuration = require("./mst_configuration");
var _mst_department = require("./mst_department");
var _mst_department_head = require("./mst_department_head");
var _mst_grade = require("./mst_grade");
var _mst_improvement_type = require("./mst_improvement_type");
var _mst_job_grade = require("./mst_job_grade");
var _mst_job_owner = require("./mst_job_owner");
var _mst_role = require("./mst_role");
var _mst_value_aspect = require("./mst_value_aspect");
var _tbl_idea = require("./tbl_idea");

function initModels(sequelize) {
  var mst_approval_level = _mst_approval_level(sequelize, DataTypes);
  var mst_area_implementation = _mst_area_implementation(sequelize, DataTypes);
  var mst_area_implementation_1 = _mst_area_implementation_1(sequelize, DataTypes);
  var mst_area_implementation_2 = _mst_area_implementation_2(sequelize, DataTypes);
  var mst_area_type = _mst_area_type(sequelize, DataTypes);
  var mst_aspect = _mst_aspect(sequelize, DataTypes);
  var mst_authorization = _mst_authorization(sequelize, DataTypes);
  var mst_configuration = _mst_configuration(sequelize, DataTypes);
  var mst_department = _mst_department(sequelize, DataTypes);
  var mst_department_head = _mst_department_head(sequelize, DataTypes);
  var mst_grade = _mst_grade(sequelize, DataTypes);
  var mst_improvement_type = _mst_improvement_type(sequelize, DataTypes);
  var mst_job_grade = _mst_job_grade(sequelize, DataTypes);
  var mst_job_owner = _mst_job_owner(sequelize, DataTypes);
  var mst_role = _mst_role(sequelize, DataTypes);
  var mst_value_aspect = _mst_value_aspect(sequelize, DataTypes);
  var tbl_idea = _tbl_idea(sequelize, DataTypes);


  return {
    mst_approval_level,
    mst_area_implementation,
    mst_area_implementation_1,
    mst_area_implementation_2,
    mst_area_type,
    mst_aspect,
    mst_authorization,
    mst_configuration,
    mst_department,
    mst_department_head,
    mst_grade,
    mst_improvement_type,
    mst_job_grade,
    mst_job_owner,
    mst_role,
    mst_value_aspect,
    tbl_idea,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
